import requests
import subprocess
import time

ajax = "https://pollingkita.com/ajax.php"

#url polling
url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"

headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "Content-Length": "279",
    "Host": "pollingkita.com",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate, br",
    "Connection": "keep-alive",
    # "Cookie": cookie
}

data = {
    'pollid': '353686', #id polling
    'selectvoteid': '3' #urutan tombol poling
}

# Define the executable path and parameters
executable_path = "nordvpn"
parameter_verbose = "-v"
parameter_connect = "connect"
parameter_disconnect = "disconnect"
#Choose server nordvpn
parameter_server = "sg" #sg:singapore

def connect_proxy():
    # Run the executable with parameters and capture the output
    result = subprocess.run([executable_path, parameter_connect, parameter_server], capture_output=True, text=True)
    # Get the output as a string
    # output = result.stdout.strip()
    # Print the output
    # print(output)
    print("Connect Proxy")

def disconnect_proxy():
    # Disconnect proxy
    # Run the executable with parameters and capture the output
    result = subprocess.run([executable_path, parameter_disconnect], capture_output=True, text=True)
    # Get the output as a string
    # output = result.stdout.strip()
    # Print the output
    # print(output)
    print("Disconnect Proxy")

c = 0
num_iterations = 10
# while True:
for i in range(num_iterations):
    print(f"Loop #{i+1}")

    connect_proxy()
    time.sleep(3)

    try:
        req = requests.get("https://ipv4.webshare.io", timeout=5)
        print(req.text)

        session = requests.Session()
        r = session.get(url)
        print("Visiting URL...")
        cookies = r.cookies.get_dict()

        # print(cookies['PHPSESSID'])
        # cookie = f"PHPSESSID={cookies['PHPSESSID']}; cookieid={cookies['cookieid']}"
        # print(cookie)

        x = session.post(ajax, headers=headers, cookies=cookies, data=data, timeout=5)
    except:
        print("error")

    try:
        # print(x.text)
        print(f"MESSAGE: {x.json()['message']}")

        if x.json()['success']:
            c += 1

        print(f"Success = {c}")
    except Exception as e:
        print(f"[EXCEPTION]: {str(e)}")
    
    disconnect_proxy()
    time.sleep(5)