

$('button[name=selectvoteid]').attr('disabled',true);
$('.option_multiple').attr('disabled',true);
$('#btnPilih').attr('disabled',true);

function shareWa() {
    var r = confirm("PERHATIAN\n\nSetelah ini, Silahkan pilih Kontak dan Group Whatsapp yang ingin anda bagikan Polling tentang KETIKA PEMILIHAN WALIKOTA BANDAR LAMPUNG 2024 DILAKUKAN HARI INI, SIAPA YANG ANDA PILIH ?  ?");
    if (r == true) {
        window.open('https://wa.me/?text=KETIKA+PEMILIHAN+WALIKOTA+BANDAR+LAMPUNG+2024+DILAKUKAN+HARI+INI%2C+SIAPA+YANG+ANDA+PILIH+%3F++%3F%0A%0ASilahkan+klik+link+https%3A%2F%2Fpollingkita.com%2Fpolling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih%3Fref%3Dwa+dan+tentukan+pilihan+anda%0A%0ATolong+forward+ke+group+dan+teman-temanmu+ya.+Terima+kasih', '_blank');
    }
} 
function shareSosmedOld(toSos){
  var textNya = $('#txtShare').text();
  if (toSos=='wa'){
        window.open(encodeURI('https://api.whatsapp.com/send?text='+textNya.replace('{thisLink}','https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih')), '_blank');
  }else if (toSos=='tw'){
        window.open(encodeURI('https://twitter.com/share?text='+textNya.replace('{thisLink}','https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih')+'&url=https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih'), '_blank');
  }else if (toSos=='fb'){
        window.open(encodeURI('https://www.facebook.com/sharer/sharer.php?u=https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih&quote='+textNya.replace('{thisLink}','https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih')), '_blank');
  }

}


function shareSosmed(toSos){
  var textNya = $('#txtShare').text();
  $('#textarea_share').text(textNya.replace('{thisLink}','https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih'));

  if (toSos=='wa'){
    $('#form_share').attr('action','https://api.whatsapp.com/send');
    $('#form_share #textarea_share').attr('name','text');
    $('#form_share #u').text('u','');
    $('#form_share').submit();
  }
  else if (toSos=='tl'){
    $('#form_share').attr('action','https://telegram.me/share/url');
    $('#form_share #textarea_share').attr('name','text');
    $('#el_u').html('<textarea name="url" id="url" cols="30" rows="10">https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih</textarea>');
    $('#form_share').submit();
  }
  else if (toSos=='tw'){
     $('#form_share').attr('action','https://twitter.com/share');
     $('#form_share #textarea_share').attr('name','text');
     $('#form_share #u').text('u','');
     $('#form_share').submit();
  }else if (toSos=='fb'){
    $('#el_u').html('<textarea name="u" id="u" cols="30" rows="10"></textarea>');
    $('#form_share').attr('action','https://www.facebook.com/sharer/sharer.php?u=https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih');
    $('#form_share #textarea_share').attr('name','quote');
    $('#form_share #u').text('https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih');
    $('#form_share').submit();
  }

}



document.getElementById("copyButton").addEventListener("click", function() {
    copyToClipboard(document.getElementById("copyTarget"));
});

function copyToClipboard(elem) {
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);
    
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }
    
    if (isInput) {
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        target.textContent = "";
    }
    return succeed;
}
var array_vote;
function callback(response) {
  array_vote = response;
}




$(document).ready(function(){
  ajaxResult('0','',false);
});

  $(window).on("throttledresize", function (event) {
    if (!jQuery.isEmptyObject(array_vote)){
      //drawChart(array_vote);
    }
  });

  function selectOption(key,el){
    $('#selectvoteid'+key).prop('checked',true);
    $(el).attr('class','btn btn-danger btn-block mb-2');
    $(el).attr('onClick','disselectOption(\''+key+'\',this)');
  }
  function disselectOption(key,el){
    $('#selectvoteid'+key).prop('checked',false);
    $(el).attr('class','btn btn-primary btn-block mb-2');
    $(el).attr('onClick','selectOption(\''+key+'\',this)');
  }
  
    $('#formVote').submit(function (e) {
         e.preventDefault();
    var selectvoteid = $('div.checkbox-group.required :checkbox:checked').length;
    var searchIDs = $("div.checkbox-group.required :checkbox:checked").map(function(){
          return $(this).val();
      }).get(); 
      
    console.log(searchIDs);

        var submit = true;
    
    if (selectvoteid<=0){
        submit = false;
      alert('Anda belum memilih salah satu pilihan');
      }
        if (submit){
      viewResult(searchIDs,'btnPilih');
      return true;
    }
    return false;
    });
function viewResult(selectvoteid,el){
  if (selectvoteid!=0){
    if (el!='btnPilih'){
      $('#btn'+selectvoteid).html('Mohon Tunggu<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>');
    }else{
      $('#'+el).html('Mohon Tunggu<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>');
    }
  } 
  else{
    $('#'+el.id).html('Mohon Tunggu<i class="fa fa-spinner fa-pulse fa-fw"></i><span class="sr-only">Loading...</span>');
  }
  
  $('button[name=selectvoteid]').attr('disabled',true);
  ajaxResult(selectvoteid,el);
}

function ajaxResult(selectvoteid, el, show_div=true){

  $.ajax({
    url : 'ajax.php',
    type: 'POST',
    data: {
        selectvoteid : selectvoteid,
pollid : '353686'
    },  
dataType: 'json',
    success: function (response) {
if (selectvoteid!=0){
if (el!='btnPilih'){
  $('#btn'+selectvoteid).html($(el).data('txt'));
}else{
  $('#'+el).html('Pilih');
}
}
else{
if (el.id=='btnRefresh'){
$('#'+el.id).html('Refresh');
}else{
$('#'+el.id).html('Lihat Hasil Polling'); 
}
}
$('button[name=selectvoteid]').attr('disabled',false);


$('button[name=selectvoteid]').attr('disabled',false);
$('.option_multiple').attr('disabled',false);
$('#btnPilih').attr('disabled',false);



if (selectvoteid!=0&&!response.success){
  $('#alert').html('<div style="margin-top:10px" class="alert alert-'+(response.success?'success':'danger')+'" role="alert">'+response.message+'</div>');  
}

var countVote=0;  
  //console.log(response); 

if (show_div){
  if (selectvoteid!=0&&response.success){
    window.location.href = 'https://pollingkita.com/index.php?mod=polling&id=353686&action=thanks';
  }
 //google.charts.setOnLoadCallback(drawChart(response));
 callback(response);
}
  $.each(response.result, function(key, value) {
    countVote = countVote + parseInt(value);
  }); 

var li='';
var txtShare='KETIKA PEMILIHAN WALIKOTA BANDAR LAMPUNG 2024 DILAKUKAN HARI INI, SIAPA YANG ANDA PILIH ?  ?\n';

$.each(response.result, function(key, value) {
  var percent = ((value*100)/countVote).toFixed(1);
  if (isNaN(percent)){
    percent = '0';
  }
  li+='<li class="list-group-item">'+key+' <span class="float-right badge badge-secondary">'+value+' Suara</span> <br><div class="progress"><div class="progress-bar progress-bar-striped bg-info" role="progressbar" style="width: '+percent+'%;" aria-valuenow="'+percent+'" aria-valuemin="0" aria-valuemax="100">'+percent+'%</div></div></li>';
  txtShare+='\n- '+key+' - '+percent+'%';
}); 
$('#bar').html(li); 
$('#txtShare').text(txtShare+'\n\nTotal suara: '+countVote+'\n\nBerikan suara anda, klik link\n{thisLink}\n\nTolong forward ke group dan teman-temanmu ya. Terima kasih'); 
$('#cVote').text(countVote); 
$('#spanCount').text(countVote); 
    }
}); 

}


