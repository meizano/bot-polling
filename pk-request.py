import requests
import random
import socket
import struct
import time

ajax = "https://pollingkita.com/ajax.php"

url = "https://pollingkita.com/menu-tentang-kami-27.html"

pollid = '353686'
selectvoteid = '3'

i = 0
c = 0
num_iterations = 10
while True:
#for i in range(num_iterations):
    print(f"Loop #{i+1}")

    try:
        session = requests.Session()
        r = session.get(url, timeout=10)
        cookies = r.cookies.get_dict()

        # print(cookies['PHPSESSID'])
        # cookie = f"PHPSESSID={cookies['PHPSESSID']}; cookieid={cookies['cookieid']}"
        # print(cookie)

        ip_address = socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))

        headers = {
            "Content-Type": "application/x-www-form-urlencoded",
            "Content-Length": "279",
            "Host": "pollingkita.com",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
            "Accept": "*/*",
            "Accept-Encoding": "gzip, deflate, br",
            "Connection": "keep-alive",
            'X-Forwarded-For': ip_address
            # "Cookie": cookie
        }

        data = {
            'pollid': pollid,
            'selectvoteid': selectvoteid
        }

        x = session.post(ajax, headers=headers, cookies=cookies, data=data, timeout=15)
        print(f"MESSAGE: {x.json()['message']}")
        if x.json()['success']:
                c += 1
            
        print(f"Success = {c}")
    except Exception as e:
        print(f"[EXCEPTION]: {str(e)}")

    i += 1
    time.sleep(2)