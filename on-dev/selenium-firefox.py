from seleniumwire import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

# Set the proxy configuration
proxy_host = 'p.webshare.io'
proxy_port = '80'
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'
host = f'{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}'

profile = webdriver.FirefoxProfile()
profile.set_preference("network.proxy.type", 1)
profile.set_preference("network.proxy.http", host) 
profile.set_preference("network.proxy.http_port", proxy_port) 
profile.set_preference("network.proxy.ssl", host) 
profile.set_preference("network.proxy.ssl_port", proxy_port) 
driver = webdriver.Firefox(firefox_profile=profile)

driver.get('https://www.myip.com/')

ip_element = driver.find_element(By.ID, "ip")
# Get the displayed IP address
ip_address = ip_element.text
print(f"Own IP address: {ip_address}")