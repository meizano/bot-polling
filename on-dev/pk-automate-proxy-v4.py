from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import requests

proxies={
    "http": "http://mgofhhmo-rotate:z12oqrjo4652@p.webshare.io:80/",
    "https": "http://mgofhhmo-rotate:z12oqrjo4652@p.webshare.io:80/"
}

try:
    #Test proxy ip rotation
    r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
    print(r.text)

    # Set up the Chrome driver
    driver = webdriver.Chrome()

    # Set the URL of the page to visit
    url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"

    # Visit the page
    driver.get(url)

    # Wait for the presence of an element that indicates the page has loaded
    wait = WebDriverWait(driver, 10)
    wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, "#btn3")))

    # Find the button element and click on it
    button_element = driver.find_element(By.CSS_SELECTOR, "#btn3")
    button_element.click()

    # Wait for the alert element to be visible
    wait.until(EC.visibility_of_element_located((By.ID, "alert")))

    # Get the text of the alert element
    alert_text = driver.find_element(By.ID, "alert").text
    print(alert_text)

    # Refresh the browser to clear cookies and session
    driver.refresh()

    # Quit the driver
    driver.quit()

except:
    print('failed')
    pass
