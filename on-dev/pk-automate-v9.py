from seleniumwire import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import requests

# Selenium Wire configuration to use a proxy
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'
proxy_host = 'p.webshare.io'
proxy_port = '80'

proxies = {
    "http": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/",
    "https": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/"
}

seleniumwire_options = {
    'suppress_connection_errors': True,
    'proxy': {
        'http': f'http://{proxy_username}:{proxy_password}@p.webshare.io:80',
        'https': f'http://{proxy_username}:{proxy_password}@p.webshare.io:80',
        'verify_ssl': False,
    },
}

# Set up the Chrome driver with Selenium Wire configuration
options = webdriver.ChromeOptions()
# options.add_argument("--headless")
options.add_argument("--incognito")
# options.add_argument("--ignore-certificate-errors")
# options.add_argument(f"--proxy=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
# options.add_argument(f"--proxy-https=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Suppress error messages


driver = webdriver.Chrome(
    ChromeDriverManager().install(),
    seleniumwire_options=seleniumwire_options,
    options=options,
)

# Test proxy ip rotation
r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
print(f"IP Proxy: {r.text}")

driver.get('https://www.myip.com/')

ip_element = driver.find_element(By.ID, "ip")
# Get the displayed IP address
ip_address = ip_element.text
print(f"Own IP address: {ip_address}")

url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"

driver.get(url)
print(f"Visit URL: {url}")

try:
    wait = WebDriverWait(driver, 10)
    wait.until(EC.presence_of_element_located((By.ID, "btn3")))

    # Find the button element and get its text
    button_element = driver.find_element(By.ID, "btn3")
    button_text = button_element.text
    print(button_text)
except Exception as e:
    print(f"Exception: {str(e)}")