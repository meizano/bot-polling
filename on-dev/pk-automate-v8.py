from seleniumwire import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import requests
import msvcrt
import logging

# Disable logging of certificate verification errors
logging.getLogger('seleniumwire').setLevel(logging.CRITICAL)

# Set the proxy configuration
proxy_host = 'p.webshare.io'
proxy_port = '80'
proxy_username = 'mgofhhmo-rotate'
proxy_password = 'z12oqrjo4652'

# Create a Proxy object
proxy = Proxy()
proxy.proxy_type = ProxyType.MANUAL
proxy.http_proxy = f'{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}'
proxy.ssl_proxy = f'{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}'

proxies = {
    "http": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/",
    "https": f"http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}/"
}

# Create desired capabilities and set the proxy
capabilities = webdriver.DesiredCapabilities.CHROME.copy()
proxy.add_to_capabilities(capabilities)

# Set the IP addresses or CIDR ranges to whitelist
whitelisted_ips = []

# Set up the Chrome driver with Selenium Wire configuration
options = webdriver.ChromeOptions()
# options.add_argument("--headless")
options.add_argument("--incognito")
options.add_argument("--ignore-certificate-errors")
# options.add_argument(f"--proxy=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
# options.add_argument(f"--proxy-https=http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Suppress error messages

# Configure the whitelist IP addresses
seleniumwire_options = {
    'whitelist_ips': whitelisted_ips,
    'suppress_connection_errors': True,
    'proxy': {
        'http': f'http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}',
        'https': f'http://{proxy_username}:{proxy_password}@{proxy_host}:{proxy_port}',
        'verify_ssl': False,
    },
}

# Repeat the code multiple times until 'S' key is pressed
# i = 0
num_iterations = 2
# while True:
for i in range(num_iterations):
    print(f"Iteration {i+1}")
    
    try:
        # Test proxy ip rotation
        r = requests.get('https://ipv4.webshare.io/', proxies=proxies, timeout=10)
        print(f"IP Proxy: {r.text}")

        # Initialize the Chrome driver with options and Selenium Wire options
        driver = webdriver.Chrome(
            ChromeDriverManager().install(),
            seleniumwire_options=seleniumwire_options,
            options=options,
            desired_capabilities=capabilities
        )
        print("Start browser.")

        # Visit a website to check the IP address
        try:
            driver.get('https://www.myip.com/')
            print(f"Visit URL: {driver.current_url}")
            
            ip_element = driver.find_element(By.ID, "ip")

            # Get the displayed IP address
            ip_address = ip_element.text
            print(f"Own IP address: {ip_address}")
        except Exception as e:
            print(f"Page navigation failed. Error: {str(e)}")

        # Set the URL of the page to visit
        url = "https://pollingkita.com/polling353686-polling-ketika-pemilihan-walikota-bandar-lampung-2024-dilakukan-hari-ini-siapa-yang-anda-pilih"

        # Store the initial URL
        initial_url = url

        # Visit the page
        try:
            driver.get(url)
            print(f"Visit URL: {url}")
        except Exception as e:
            print(f"Page navigation failed. Error: {str(e)}")

        try:
            # Wait for the presence of the button element
            wait = WebDriverWait(driver, 10)
            wait.until(EC.presence_of_element_located((By.ID, "btn3")))

            # Find the button element and get its text
            button_element = driver.find_element(By.ID, "btn3")
            button_text = button_element.text
            print(button_text)
        except Exception as e:
            print(f"Exception: {str(e)}")

        try:
            # Find the button element and click on it
            button_element.click()
            print("Button clicked successfully.")
        except Exception as e:
            print(f"Exception: {str(e)}")
        
        try:
            # Wait for the alert element to be visible
            wait.until(EC.visibility_of_element_located((By.ID, "alert")))

            # Get the text of the alert element
            alert_text = driver.find_element(By.ID, "alert").text
            print(alert_text)
        except Exception as e:
            print(f"Error: {str(e)}")
            pass

        # Check if the URL has changed
        if driver.current_url != initial_url:

            print(f"Sukses: {driver.current_url}")
       
        # Refresh the browser to clear cookies and session
        driver.refresh()
        print("Clear cookies and session.")
        
        # Quit the driver
        driver.quit()
        print("Close browser.")

    except:
        print('failed')
        pass

    i += 1

    # Check for keypress event
    if msvcrt.kbhit():
        key = msvcrt.getch().decode()
        if key.lower() == 's':
            break