from seleniumwire import webdriver
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import re
import psycopg2

db_name = "beta_sigap"
db_host = "149.28.152.242"
db_user = "postgres"
db_pass = "rtsp2022"
db_port = 5432

options = webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("--incognito")
options.add_argument("--ignore-certificate-errors")
options.add_argument("--disable-blink-features=AutomationControlled")
options.add_experimental_option('excludeSwitches', ['enable-logging'])  # Suppress error messages
#Linux
options.add_argument("disable-infobars")
options.add_argument("--disable-extensions")
#options.add_argument("--disable-gpu") #windows only
options.add_argument("--disable-dev-shm-usage")
options.add_argument("--no-sandbox")
options.add_argument("--remote-debugging-port=9222")

# Configure the whitelist IP addresses
seleniumwire_options = {
    'suppress_connection_errors': True,
}

# uc.install()
driver = webdriver.Chrome(
            # ChromeDriverManager().install(),
            "chromedriver", #Linux only
            seleniumwire_options=seleniumwire_options,
            options=options,
        )

wait = WebDriverWait(driver, 5)
            

try:
    url_1 = "https://pollingkita.com/index.php?mod=poll_result&id=353686"
    # url = "https://wwsw.datacamp.com/users/sign_in"

    pollid = url_1.split('&id=', 1)[1]

    driver.get(url_1) 
    print(f"Visit URL: {driver.current_url}")

    wait.until(EC.presence_of_element_located((By.XPATH, "//li[@class='list-group-item']")))
    # wait.until(EC.presence_of_element_located((By.XPATH, "//span[@class='float-right badge badge-secondary']")))

    li = driver.find_elements(By.XPATH, "//li[@class='list-group-item']")
    # span = driver.find_elements(By.XPATH, "//span[@class='float-right badge badge-secondary']")

    connection = psycopg2.connect(database=db_name,
                                host=db_host,
                                user=db_user,
                                password=db_pass,
                                port=db_port)
    cursor = connection.cursor()
    print("Connect database")

    for i in range(len(li)):
        text = li[i].text
        nama = text.split('\n', 1)[0]
        suara = text.split('\n', 1)[1]
        suara = int(re.search("\d+", suara)[0])
        pros = text.split('\n\n', 1)[1]
        print(f"{pros} {nama}: {suara}");

        # Insert into database
        postgres_insert_query = """ INSERT INTO polling (POLLID, PROS, NAME, COUNT) VALUES (%s,%s,%s,%s)"""
        record_to_insert = (pollid, pros, nama, suara)
        cursor.execute(postgres_insert_query, record_to_insert)

        connection.commit()

    count = cursor.rowcount
    print(count, "Log stored")
    cursor.close()
    connection.close()
    print("Close database")

except Exception as e:
    print(f"[EXCEPTION]: {str(e)}")